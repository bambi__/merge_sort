class Node:

    def __init__(self, key):
        self.key = key
        self.right = None


class LinkedList:

    # Function to initialize head
    def __init__(self):
        self.head = None
        self.last = None

    def sortedInsert_list(self, new_node):

        # Special case for the empty linked list
        if self.head is None:
            new_node.right = self.head
            self.head = new_node
            self.last = new_node

            # Special case for head at end
        elif self.head.key >= new_node.key:
            new_node.right = self.head
            self.head = new_node

        else:

            # Locate the node before the point of insertion
            current = self.last
            while (current.right is not None and
                   current.right.key <= new_node.key):
                current = current.right

            new_node.right = current.right
            current.right = new_node
            self.last = new_node


    def sortedInsert(self, new_node):

        # Special case for the empty linked list
        if self.head is None:
            new_node.right = self.head
            self.head = new_node

            # Special case for head at end
        elif self.head.key >= new_node.key:
            new_node.right = self.head
            self.head = new_node

        else:

            # Locate the node before the point of insertion
            current = self.head
            while (current.right is not None and
                   current.right.key <= new_node.key):
                current = current.right
            start = time.time()
            new_node.right = current.right
            current.right = new_node
            end = time.time()
            inserting_sup.append(end-start)




    def get(self, x):


        current = self.head

        # loop till current not equal to None
        while current != None:
            if current.key == x:
                return current.key  # data found

            current = current.right

        return False  # Data Not found


# testing for implementation of algorithm
node = Node(1)

linked_ = LinkedList()
new_node = Node(1)
linked_.sortedInsert(new_node)

# measuring time complexity
import time
import random
from random import sample
import merge_sort

anzahl=[100000, 250000, 500000]
creation_ = []
getting_ = []
getting_total_ = []
inserting_ = []
inserting_total_ = []
population_ = list(range(0,25000000))

for e in anzahl:
    li = sample(population_, e)
    li = sorted(li)
    start = time.time()
    linked_ = LinkedList()
    n = 1
    while n < len(li):
        new_node = Node(li[n])
        linked_.sortedInsert_list(new_node)
        n = n +1
    end = time.time()
    print("Creation of", e, "Elements takes", end-start)
    creation_.append(end-start)

    y = 1
    sum_of_time = 0
    x = random.choices(li, k=101)

    start = time.time()
    while y <= 100:
        linked_.get(x[y])
        y = y + 1

    end = time.time()
    sum_of_time = end - start
    average_time = sum_of_time / y
    getting_total_.append(end - start)
    getting_.append(average_time)

    y = 1

    inserting_sup = []
    z = sample(population_, 101)

    while y <= 100:
        new_node = Node(z[y])
        linked_.sortedInsert(new_node)
        y = y + 1



    inserting_total_.append(sum(inserting_sup))
    inserting_.append(sum(inserting_sup) / y)


print(creation_)
print(getting_)
print(inserting_)

import pandas as pd

anzahl = pd.Series(anzahl)
getting_ = pd.Series(getting_)
inserting_ = pd.Series(inserting_)
creation_ = pd.Series(creation_)
getting_total_ = pd.Series(getting_total_)
inserting_total_ = pd.Series(inserting_total_)


df = pd.concat([anzahl, getting_, inserting_], axis=1)
df = pd.DataFrame ({'elements':anzahl, 'getting_':getting_, 'inserting_': inserting_, 'creation_': creation_,
                    'getting_total_': getting_total_, 'inserting_total_': inserting_total_})

print(df)

import matplotlib.pyplot as plt


plt.xlabel('Number of Elements')
plt.ylabel('Time in Seconds')

plt.plot(anzahl, inserting_, label='Inserting Average')
plt.plot(anzahl, getting_, label='Getting Average')
plt.legend(loc='upper left', title='Legend', frameon=False)
plt.show()































